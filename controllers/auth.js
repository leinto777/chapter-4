const { users_game } = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const { JWT_SIGNATURE_KEY } = process.env;

module.exports = {
  register: async (req, res, next) => {
    const { username, password } = req.body;

    try {
      const existUser = await users_game.findOne({
        where: { username: username },
      });
      if (existUser) {
        return res.status(400).json({
          status: false,
          message: "Email already taken",
        });
      }

      const encryptedPassword = await bcrypt.hash(password, 10);

      const user = await users_game.create({
        username,
        password: encryptedPassword,
      });

      return res.status(200).json({
        status: false,
        message: "success",
        data: {
          username: user.username,
          password: user.password,
        },
      });
    } catch (error) {
      next(err);
    }
  },
  login: async (req, res, next) => {
    try {
      /*
      - get req body (email, password)
      - cek database email ada atau tidak
      - if not => balikin error 404
      - if true => cek hashPassword
      - generate token (JWT => jsonwebtoken)
      - return token
      */

      const { username, password } = req.body;

      const user = await users_game.findOne({ where: { username: username } });
      if (!user) {
        return res.status(400).json({
          status: false,
          message: "email not found!",
        });
      }

      const correct = await bcrypt.compare(password, user.password);
      if (!correct) {
        return res.status(400).json({
          status: false,
          message: "email/password salah",
        });
      }

      //generate
      payload = {
        id: user.id,
        username: user.username,
        password: user.password,
      };

      const token = jwt.sign(payload, JWT_SIGNATURE_KEY);

      return res.status(200).json({
        status: false,
        message: "success",
        data: {
          // name: user.name,
          // email: user.email,
          token: token,
        },
      });
    } catch (error) {}
  },
  whoami: (req, res, next) => {
    const user = req.user;

    try {
      return res.status(200).json({
        status: false,
        message: "success",
        data: user.username,
      });
    } catch (error) {
      next(error);
    }
  },
  changePassword: async (req, res, next) => {
    try {
      // const user = req.user;
      const { password, newPassword, confirmPassword } = req.body;
      console.log(newPassword);
      if (newPassword != confirmPassword) {
        return res.status(400).json({
          status: false,
          message: "Password doesn't match",
        });
      }
      const user = await users_game.findOne({ where: { id: req.user.id } });
      if (!user) {
        return res.status(400).json({
          status: false,
          message: "User not found",
        });
      }

      const correct = await bcrypt.compare(password, user.password);
      if (!correct) {
        return res.status(400).json({
          status: false,
          message: "Old password doesn't match",
        });
      }

      const encryptedPassword = await bcrypt.hash(newPassword, 10);
      const updatePassword = await users_game.update(
        {
          password: encryptedPassword,
        },
        {
          where: {
            id: user.id,
          },
        }
      );

      return res.status(200).json({
        status: false,
        message: "succes change password",
        data: updatePassword,
      });
    } catch (error) {}
  },
};
