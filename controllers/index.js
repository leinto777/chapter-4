const auth = require("./auth");
const bio = require("./user_game_biodata");
const history = require("./user_history_games");

module.exports = {
  exception: (err, req, res, next) => {
    res.render("server-error", { error: err.message });
  },
  notFound: (req, res, next) => {
    res.render("not-found");
  },
  // userGames,
  auth,
  bio,
  history,
};
