const { user_game_biodata } = require("../models");

module.exports = {
  createBio: async (req, res, next) => {
    try {
      const user = req.user;
      const { bio } = req.body;
      if (!user.username) {
        return res.status(404).json({
          status: "failed",
          message: "not found",
          data: null,
        });
      }
      const userCreate = await user_game_biodata.create({
        username: user.username,
        bio,
      });
      return res.status(200).json({
        status: "Success",
        message: "Create Bio",
        data: userCreate,
      });
    } catch (error) {
      next(error);
    }
  },
  updateBio: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { username, bio } = req.body;
      const user = await user_game_biodata.findOne({
        where: {
          id: id,
        },
      });
      console.log(user.bio);
      if (!user) {
        return res.status(400).json({
          status: false,
          message: "empty",
        });
      }
      const updated = await user_game_biodata.update(
        {
          username,
          bio,
        },
        {
          where: {
            id,
          },
        }
      );
      console.log(user.bio);
      return res.status(200).json({
        message: "success",
        data: user,
        info: updated,
      });
    } catch (error) {
      next(error);
    }
  },

  deleteBio: async (req, res, next) => {
    try {
      // const { id } = req.params;
      const user = req.user;
      if (!user.username) {
        return res.status(404).json({
          status: "failed",
          message: "not found",
          data: null,
        });
      }
      const userDetail = await user_game_biodata.findOne({
        where: {
          username: user.username,
        },
      });
      const deleted = await user_game_biodata.destroy({
        where: {
          username: user.username,
        },
      });

      return res.status(200).json({
        status: "Success",
        message: "Delete Data",
        data: deleted,
      });
    } catch (error) {
      next(error);
    }
  },
  infoAllUser: async (_req, res, next) => {
    try {
      const usersBio = await user_game_biodata.findAll();
      if (usersBio.length <= 0) {
        res.status(404).json({
          status: "failed",
          message: "not found",
          data: null,
        });
      }
      return res.status(200).json({
        status: "Success",
        message: "Success get all data",
        data: usersBio,
      });
    } catch (error) {
      next(error);
    }
  },
};
