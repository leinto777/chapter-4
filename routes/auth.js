const express = require("express");
const router = express.Router();
const controller = require("../controllers");
const mid = require("../helper/middleware");

// router.get("/", controller.userGames.getUsers);
// router.get("/:id", controller.userGames.getDetailUser);
// router.post("/", controller.userGames.createUser);
// router.put("/:id", controller.userGames.updateUser);
// router.delete("/:id", controller.userGames.deleteUser);

router.post("/auth/register", controller.auth.register);
router.post("/auth/login", controller.auth.login);
router.get("/auth/whoami", mid.mustLogin, controller.auth.whoami);
router.post("/auth/change", mid.mustLogin, controller.auth.changePassword);

module.exports = router;
