const express = require("express");
const router = express.Router();
const auth = require("./auth");
const bio = require("./user_game_biodata");
const history = require("./user_history_games");

router.use("/usersgame", auth);
router.use("/usersgame", bio);
router.use("/usersgame", history);

module.exports = router;
