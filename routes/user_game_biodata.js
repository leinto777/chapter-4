const express = require("express");
const router = express.Router();
const controller = require("../controllers");
const mid = require("../helper/middleware");

router.post("/bio/create-bio", mid.mustLogin, controller.bio.createBio);
router.put("/bio/update/:id", mid.mustLogin, controller.bio.updateBio);
router.delete("/bio/delete-bio", mid.mustLogin, controller.bio.deleteBio);
router.get("/bio/getAllBio", mid.mustLogin, controller.bio.infoAllUser);

module.exports = router;
